/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sfont.hpp>

bool CSFontManager::IsCharExist (u_char c1, u_char c2)
{
    if (aCharFontInfo == NULL)
        return false;
    for (int i = 0; i < total; i++)
        if (aCharFontInfo[i].c1 == c1 && 
            aCharFontInfo[i].c2 == c2)
            return true;
    return false;
}

bool CSFontManager::DoAddCharFont (u_char c1, u_char c2, u_char *buf)
{
    if (aCharFontInfo == NULL)
    {
        aCharFontInfo = (CharFontInfo *) malloc (sizeof (CharFontInfo));
    }
    else
        aCharFontInfo = (CharFontInfo *) 
            realloc (aCharFontInfo, (total + 1) * sizeof (CharFontInfo));

    if (aCharFontInfo == NULL)
        return false;

    aCharFontInfo[total].c1 = c1;
    aCharFontInfo[total].c2 = c2;
    memcpy (aCharFontInfo[total].buf, buf, FONT_SIZE);
    total ++;

    return true;
}

CSFontManager::CSFontManager ()
{
    total = 0;
    aCharFontInfo = NULL;
}

CSFontManager::~CSFontManager ()
{
    if (aCharFontInfo != NULL)
    {
        for (int i = 0; i < total; i++)
            free (aCharFontInfo[i].buf);
        free (aCharFontInfo); 
    }
}

bool CSFontManager::AddCharFont (u_char c1, u_char c2, u_char *buf)
{
    if (IsCharExist (c1, c2) == true)
        return true;
    // printf ("%c%c\n", c1, c2);
    return DoAddCharFont (c1, c2, buf);
}

static int fcomp (const void *p1, const void *p2)
{
    CharFontInfo *a1 = (CharFontInfo *) p1;
    CharFontInfo *a2 = (CharFontInfo *) p2; 
    unsigned int n1, n2;
    n1 = (unsigned int) a1->c1 * (unsigned int) a1->c2;
    n2 = (unsigned int) a2->c1 * (unsigned int) a2->c2;
    if (n1 > n2)
        return 1;
    else if (n1 == n2)
        return 0;
    else 
        return -1;
}

bool CSFontManager::SortCharFonts ()
{
    if (aCharFontInfo != NULL)
        qsort (aCharFontInfo, total,  sizeof (CharFontInfo), fcomp);
}

bool CSFontManager::bSaveToFile (char *szFilename, int coding)
{
    FILE *fp;
    if (aCharFontInfo == NULL)
        return false;

    // SortCharFonts ();

    if ((fp = fopen (szFilename, "wt")) == NULL)
        return false;

    /* Write Head */

//    fprintf (fp, "typedef unsigned char u_char;\n\n");

    bWriteIndexFile (fp);
    bWriteDataFile (fp);

#if 0 
    switch (coding)
    {
        case XL_DB_GB:
            bSaveGB_C (szCFile);            
            break;
        case XL_DB_BIG5:
            bSaveGB_C (szCFile);            
            break;
        case XL_DB_JIS:
            bSaveGB_C (szCFile);            
            break;
        case XL_DB_KSCM:
            bSaveGB_C (szCFile);            
            break;
        case XL_DB_GBK:
            bSaveGB_C (szCFile);            
            break;
        default:
            return false;
    }
#endif 
//    fprintf (fp, "#endif\n");
    fclose (fp);
}

bool CSFontManager::bWriteIndexFile (FILE *fp)
{
    fprintf (fp, "typedef struct CharFontInfo_T { \n");
    fprintf (fp, "    int key; \n");
    fprintf (fp, "    int num; \n");
    fprintf (fp, "} CharFontInfo; \n\n");

    fprintf (fp, "#define MAX_SFONT       %d\n", total);

    fprintf (fp, "CharFontInfo aCharFontInfo[%d] = {\n", total);
    for (int i = 0; i < total; i++)
    {
        printf ("%c%c\n", 
                     aCharFontInfo[i].c1, 
                     aCharFontInfo[i].c2);
        fprintf (fp, "\n{/* %c%c */  0x%02x%02x, %d }, \n", 
		     aCharFontInfo[i].c1, 
		     aCharFontInfo[i].c2,
		     aCharFontInfo[i].c1,
		     aCharFontInfo[i].c2,
		     i + 1);
    }
    fprintf (fp, "\n};\n");
}

bool CSFontManager::bWriteOneFontInfo 
     (FILE *fp, u_char c1, u_char c2, unsigned char buf[FONT_SIZE])
{
    fprintf (fp, "\n /* %c%c */ \n", c1, c2);
    for (int j = 0; j < FONT_SIZE; j++)
    {
        if (j % 8 == 0 && j != 0)
            fprintf (fp, "\n    ");
        fprintf (fp, "0x%x, ", buf[j]);
    }
}

bool CSFontManager::bWriteDataFile (FILE *fp)
{
    unsigned char buf[FONT_SIZE];
    fprintf (fp, "#define MAX_SFONT_DATA           %d\n\n",  
             FONT_SIZE * (total + 1));

    fprintf (fp, "static unsigned char FontData[MAX_SFONT_DATA] = {");

    memset (buf, 0, FONT_SIZE);
    bWriteOneFontInfo (fp, 0, 0, buf);

    for (int i = 0; i < total; i++)
        bWriteOneFontInfo (fp, aCharFontInfo[i].c1, aCharFontInfo[i].c2, 
                          (unsigned char *) aCharFontInfo[i].buf); 
    fprintf (fp, "\n};\n");
}

