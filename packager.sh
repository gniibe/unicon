#!/bin/bash
PKGNAME=unicon
VERSION=3.0.4
UNICONPKG=$PKGNAME-$VERSION-`date +%Y%m%d`.tar.bz2
make distclean
cd ..
tar Ivcf $UNICONPKG $PKGNAME-$VERSION
rpm -ta $UNICONPKG
