/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2002
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

/* hzinput.h -- definitions used in input methods */

#ifndef HZINPUT_H
#define HZINPUT_H

#define MAX_INPUT_LENGTH        17
#define MAGIC_NUMBER            "CCEGB"
#define CIN_ENAME_LENGTH        24
#define CIN_CNAME_LENGTH        16
#define MAX_PHRASE_LENGTH       20
#define SELECT_KEY_LENGTH       16
#define END_KEY_LENGTH          16

#define InputAreaX              8
  // 0-7 input method name, 8 input chars
#define IntCode_SelectionX      13
#define SelectionX             (InputAreaX + MAX_INPUT_LENGTH + 1)
// 26
   // 8-23  16 max input chars
/* #define SelectionXMax           78  */
extern int SelectionXMax;

/* key of toggle input method */

#define NR_INPUTMETHOD 9
 /* 0-9, 0 PinYin, 9 IntCode */

typedef struct
{
  unsigned long key1;		/* sizeof(ITEM=12) */
  unsigned long key2;
  unsigned short ch;
  unsigned short frequency;
}
ITEM;

typedef struct
{
  char magic_number[sizeof (MAGIC_NUMBER)];	/* magic number */
  char ename[CIN_ENAME_LENGTH];	/* ascii name */
  char cname[CIN_CNAME_LENGTH];	/* prompt */
  char selkey[SELECT_KEY_LENGTH];	/* select keys */

  char last_full;		/* last full code need a more SPACE? */
  int TotalKey;			/* number of keys needed */
  int MaxPress;			/* Max len of keystroke */
  int MaxDupSel;		/* how many keys used to select */
  int TotalChar;		/* Defined characters */

  unsigned char KeyMap[128];	/* Map 128 chars to 64(6 bit) key index */
  unsigned char KeyName[64];	/* Map 64 key to 128 chars */
  unsigned short KeyIndex[64];	/* 64 key first index of TotalChar */

  int PhraseNum;		/* Total Phrase Number */
  FILE *PhraseFile;		/* *.tab.phr Phrase File */
  FILE *AssocFile;		/* *.tab.lx LianXiang File */
  ITEM *item;			/* item */
}
hz_input_table;

typedef struct __HZ_INPUT_TABLE__
{
  int IsHanziInput;		/* 0 for ascii, 
				   1 for hanzi input */
  int IsFullChar;		/* 0 for half char, 1 for full char */
  int IsFullComma;		/* 0 for half comma, 1 for full comma */
  int IsSysMenu;		/* 0 is no menu, 1 is menu */
  int IsHelpMenu;		/* 0 is no menu, 1 is menu */
  int current_method;
  hz_input_table *cur_table;
  char seltab[16][MAX_PHRASE_LENGTH];

  int CurSelNum;		/* Current Total Selection Number */
  unsigned long InpKey[MAX_INPUT_LENGTH], save_InpKey[MAX_INPUT_LENGTH];

  /* Input key buffer */
  int InputCount, InputMatch, StartKey, EndKey;
  int save_StartKey, save_EndKey,
    save_MultiPageMode, save_NextPageIndex, save_CurrentPageIndex;

  int NextPageIndex, CurrentPageIndex, MultiPageMode;

  unsigned long val1, val2, key1, key2;
  int IsAssociateMode;
  int CharIndex[15];		// starting index of matched char/phrases
  int UseAssociateMode;
  int IsFullCommaInChinese;
  int IsFullCommaInEnglish;
  int IsFullCharBackup;
  int IsHanziInputBackup;
  int MaxSelectLen;
  int MAX_SEL_LENGTH;
}
HzInputTable_T;

#endif
