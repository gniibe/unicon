/* $Id$ */
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <pinyin.h>
#include "safestring.h"

PinYin pytab[26][MAX_EACH_PY];
char hztab[MAX_PY_NUM][MAX_EACH_HZ];

int
LoadTable (char *pathname)
{
  FILE *stream;
  char str[250], *strpy, *strhz;
  int i = 1, j = 0, lastpy = 0, curpy;

  if ((stream = fopen (pathname, "r")) == NULL)
    {
      fprintf (stderr, "%s file not found\n", pathname);
      exit (1);
    }

  while (!feof (stream))
    {
      if (fgets (str, 250, stream) != NULL)
	{
	  strpy = strtok (str, " \f\n\r\t\v");
	  strhz = strtok (NULL, " \f\n\r\t\v");
	  safe_strncpy (hztab[i], strhz, MAX_EACH_HZ);

	  curpy = strpy[0] - 'a';
	  if (curpy != lastpy)
	    j = 0;
	  safe_strncpy (pytab[curpy][j].py, strpy, MAX_PY_LEN);
	  pytab[curpy][j].key = i;
	  lastpy = curpy;

	  i++, j++;
	}
    }
  fclose (stream);
  return 0;
}

int
PrintOutPhrase (char *infile, char *pathname)
{
  FILE *out, *in;
  u_char len;
  u_char key[MAX_PHRASE_LEN + 1], str[2 * MAX_PHRASE_LEN + 1],
    tmppy[10], pinyin[70];
  char tab = '\011', *p;
  int i, j, k, size, l, m, fsize;
  u_char freq;
  Phrase *p0;
  u_short count, keyint, keytmp;
  char *file;

  if ((out = fopen (pathname, "wb")) == NULL)
    {
      fprintf (stderr, "Couldn't open \"%s\".\n", pathname);
      exit (1);
    }

  if ((in = fopen (infile, "rb")) == NULL)
    {
      fprintf (stderr, "Couldn't open \"%s\".\n", pathname);
      exit (1);
    }

  if (fseek (in, -4, SEEK_END) == -1 ||
      fread (&fsize, sizeof (int), 1, in) != 1 || fsize != ftell (in) - sizeof (int))	// error!!
    {
      fprintf (stderr, "%s is not a valid pinyin phrase file.\n", infile);
      exit (1);
    }

  if ((file = (char *) malloc (fsize)) == NULL)
    {
      fprintf (stderr, "Not enough memory!\n");
      exit (1);
    }

  fseek (in, 0, SEEK_SET);
  fread (file, fsize, 1, in);

  p = file;
  for (i = 1; i < MAX_PY_NUM; i++)
    {
      count = *(u_short *) p;
      p += sizeof (u_short);
      if (count == 0)
	continue;

      for (j = 0; j < count; j++)
	{
	  p0 = (Phrase *) p;
	  len = p0->len;
	  pinyin[0] = '\0';
	  for (k = 0; k < len; k++)
	    {
	      keyint = (u_short) p0->key[0];
	      keyint = (keyint << (8 - k)) & 0x0100;
	      keyint += (u_short) p0->key[k + 1];
	      for (l = 0; l < 26; l++)
		for (m = 0; keytmp = pytab[l][m].key; m++)
		  if (keyint == keytmp)
		    {
		      snprintf (tmppy, sizeof (tmppy), "%s ", pytab[l][m].py);
		      break;
		    }
	      if (strlen (pinyin) + strlen (tmppy) + 1 < sizeof (pinyin))
		strcat (pinyin, tmppy);
	      else
		{
		  fprintf (stderr, "buffer overrun\n");
		  abort ();
		}
	    }

	  for (k = 0; k < p0->count; k++)
	    {
	      if (sizeof (str) < (size_t) (2 * len + 1))
		{
		  fprintf (stderr, "buffer overrun\n");
		  abort ();
		}
	      else
		memcpy (str, p0->key + len + 1 + (2 * len + 1) * k, 2 * len);
	      str[2 * len] = '\0';
	      freq = p0->key[len + 1 + (2 * len + 1) * k + 2 * len];
	      fprintf (out, "%s %s %d\n", str, pinyin, freq);
	    }
	  p += 2 + len + 1 + (2 * len + 1) * p0->count;
	}
    }				// MAX_PY

  fclose (in);
  fclose (out);
  return 1;
}

int
main (int argc, char **argv)
{
  if (argc != 3)
    {
      fprintf (stderr, "usage: %s <input_name> <output_name>\n", argv[0]);
      return 1;
    }

  if(access("./pinyin.map", R_OK)==0)
  	LoadTable ("./pinyin.map");
  else if(access("/usr/lib/unicon/modules/cce/dict/pinyin.map", R_OK)==0)
	LoadTable("/usr/lib/unicon/modules/cce/dict/pinyin.map");
  else if(access("/usr/local/lib/unicon/modules/cce/dict/pinyin.map", R_OK)==0)
	LoadTable("/usr/local/lib/unicon/modules/cce/dict/pinyin.map");
  else printf("Sorry, couldn't find pinyin.map!\n"), exit(-1);

  PrintOutPhrase (argv[1], argv[2]);

  return 0;			// no error
}
