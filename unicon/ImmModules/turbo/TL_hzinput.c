/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2002
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */


/* Imm Standard Interfaces */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <xl_hzinput.h>
#include <ImmModule.h>
#include <xl_sysphrase.h>
#include <xl_phrase.h>

extern int TL_KeyFilter (HzInputTable_T * p, u_char key, char *buf, int *len);
extern char *szGetSelItem (HzInputTable_T *pClient, int vv);
extern hz_input_table *TL_LoadMethod (char *szFileName);
extern void TL_InputInit (HzInputTable_T *p);
extern int TL_UnloadMethod (hz_input_table *q);
extern int TL_ConfigureInputArea (HzInputTable_T *p, int SelectionLen);
extern int TL_GetInputDisplay (HzInputTable_T * p, char *buf);
extern int TL_GetSelectDisplay (HzInputTable_T * p, char *buf);

void SetPhraseBuffer (PhraseItem *p, char *buf, int buflen)
{
    char *p1 = buf;

    p->szKeys = p1;
    p1 += 32;

    p->KeyLen = (u_char *) p1;
    p1 += sizeof (*p->KeyLen);

    p->frequency = (freq_t *) p1;
    p1 += sizeof (freq_t);

    p->szPhrase = p1;
}

static IMM_CLIENT *IMM_open (char *szFileName, long type)
{
    HzInputTable_T *p;
    IMM_CLIENT *q;
    hz_input_table *a;

    // printf ("IMM_Open (%s, %ld)\n", szFileName, type);
    a = TL_LoadMethod (szFileName);
    if (a == NULL)
        return NULL;
    
    p = (HzInputTable_T *) malloc (sizeof (HzInputTable_T));
    if (p == NULL)
        return NULL;

    q = (IMM_CLIENT *) malloc (sizeof (IMM_CLIENT));
    if (q == NULL)
    {
        free (p);
        free (a);
        return NULL;
    };
    SetPhraseBuffer (&q->m, q->buf, sizeof (q->buf));
    q->pImmClientData = (void *) p;
    TL_InputInit (p);
    p->cur_table = a;
    ResetInput (p);
    strcpy (p->szTabFileName, szFileName);
    switch (type)
    {
        case IMM_LC_GB2312:
        case IMM_LC_GBK:
            strcpy (p->szSysPhraseFile, UNICON_LIB"/modules/turbo/tl_sysphrase.gb.bin");
            break;
        case IMM_LC_BIG5:
            strcpy (p->szSysPhraseFile, UNICON_LIB"/modules/turbo/tl_sysphrase.big5.bin");
            break;
        case IMM_LC_KSC5601:
        case IMM_LC_JISX0212:
            printf ("IMM_open::type is not implemented \n");
            exit (0);
        default:
            printf ("IMM_open::wrong type \n");
            exit (0);
    }
    p->pSysPhrase = LoadSystemPhrase (p->szSysPhraseFile, type);
    return q;
}

static int IMM_save (IMM_CLIENT *p, char *szFileName)
{
    HzInputTable_T *pClient = (HzInputTable_T *) p->pImmClientData; 
    // DumpLoadInputMethod (pClient->cur_table, "test2.txt");
    ResortPhraseFreq (pClient);
    return SaveLoadInputMethod (pClient->cur_table, szFileName);
}

static int IMM_close (IMM_CLIENT *p)
{
    HzInputTable_T *pClient = (HzInputTable_T *) p->pImmClientData; 
    HzInputTable_T *a;
    // printf ("Imm_close (%ld) \n", (long) p);
    TL_UnloadMethod (pClient->cur_table);
    a = (HzInputTable_T *) p->pImmClientData;
    UnloadSystePhrase (a->pSysPhrase);
    free (p->pImmClientData);
    free (p);
    return 1;
}

/* Indepent Modules support */
static int IMM_KeyFilter (IMM_CLIENT *p, u_char key, char *buf, int *len)
{
    return TL_KeyFilter ((HzInputTable_T *) p->pImmClientData, key, buf, len);
}

/* Input Area Configuration & Operation */
static int IMM_ConfigInputArea (IMM_CLIENT *p, int SelectionLen)
{
    return TL_ConfigureInputArea (
           (HzInputTable_T *) p->pImmClientData, SelectionLen);
}

static int IMM_GetInputDisplay (IMM_CLIENT *p, char *buf, long buflen)
{
    return TL_GetInputDisplay (
           (HzInputTable_T *) p->pImmClientData, buf);
}

static int IMM_GetSelectDisplay (IMM_CLIENT *p, char *buf, long buflen)
{
    return TL_GetSelectDisplay (
           (HzInputTable_T *) p->pImmClientData, buf);
}

int IMM_ResetInput (IMM_CLIENT *p)
{
    char buf[32];
    int len;
    /* Send Esc */
    TL_KeyFilter ((HzInputTable_T *) p->pImmClientData, '\033', buf, &len);
    return 1;
}

PhraseItem * IMM_pGetItem (IMM_CLIENT *p, u_long n)
{
    HzInputTable_T *pClient = (HzInputTable_T *) p->pImmClientData; 
    char *s = szGetSelItem (pClient, (int) n);
    if (s == NULL)
         return NULL;
    strcpy (p->m.szPhrase, s);
    return &p->m;
}

/* Phrase Operation */
static int IMM_AddPhrase (IMM_CLIENT *q, PhraseItem *p)
{
    HzInputTable_T *pClient = (HzInputTable_T *) q->pImmClientData; 
    return AppendPhrase (pClient, p->szKeys, p->szPhrase);
}

static int IMM_ModifyPhraseItem (IMM_CLIENT *p, long n, PhraseItem *pItem)
{
    return 1;
}

static int IMM_Flush (IMM_CLIENT *p)
{
    HzInputTable_T *pClient = (HzInputTable_T *) p->pImmClientData; 
    IMM_save (p, pClient->szTabFileName);
    TL_SaveAllPhrase (pClient->pSysPhrase, pClient->szSysPhraseFile);
    // TL_DumpAllPhrase ("test.txt");
    return 1;
}

#ifdef  __cplusplus
extern "C" 
#endif
struct ImmOperation ImmOp_Ptr = {
    "ƴ��", 
    "TL Chinese Input Version 1.0",
    "Author:He Rui",
    IMM_CCE | (IMM_LC_ALL) << 24,
    IMM_open,
    IMM_save,
    IMM_close,

    /* Indepent Modules support */
    IMM_KeyFilter,
    IMM_ResetInput,

    /* Input Area Configuration & Operation */
    IMM_ConfigInputArea,
    IMM_GetInputDisplay,
    IMM_GetSelectDisplay,
    IMM_pGetItem,
    IMM_AddPhrase,
    IMM_ModifyPhraseItem,
    IMM_Flush,
};

