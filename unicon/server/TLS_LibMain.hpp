/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#ifndef __LIBMAIN_HPP__
#include <stdlib.h>
#include <TLS_HzInput.hpp>
#include <TLS_ImmOp.hpp>
#include <Phrase.h>

class TLS_CServerMain
{
private:
    char str[256], str1[256];
    TLS_CHzInput *pTLS_CHzInput;
    TLS_CHzInput *aTLS_CHzInput[64];
    ImmOperation * pImmOp;
private:
    char *DupBufPhrase (PhraseItem *Old, char *buf, PhraseItem *New);
public:
    TLS_CServerMain ();
    ~TLS_CServerMain ();

    long OpenServer (char *ImmModule, char *ImmTable, long type);
    int  CloseServer (long handle);

    int  ResetInput (long handle);
    int  KeyFilter (long handle, u_char key, char *buf, int *len);
    int  SetInputMode (long handle, long mode);

    int  ConfigInputArea (long  handle, long TotalSelection);
    int  GetInputDisplay (long handle, char *buf, int buflen);
    int  GetSelectDisplay (long handle, char *buf, int buflen);

    int  SetPhraseItem (long handle, long n, 
                        char *szCode, char *szPhrase, u_long freq);
    int  AddUserPhrase (long handle, 
                        char *szCode, char *szPhrase, u_long freq);
    int  FlushUserPhrase (long handle);
};

#ifdef  __cplusplus
extern "C" {
#endif

extern long TCP_Connect (char *szIpAddr, short Port);
extern int TCP_Disconnect (long handle);
extern int LibOpen ();
extern int LibRelease ();

#ifdef  __cplusplus
}
#endif

#endif

