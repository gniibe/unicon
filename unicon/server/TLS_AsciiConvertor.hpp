
/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#ifndef __ASCIICONVERTOR_HPP__
#define __ASCIICONVERTOR_HPP__

#include <stdlib.h>
#include <ctype.h>
#include <X11/Xlib.h>
#include <X11/keysym.h>

#ifndef WCH_SIZE
#define WCH_SIZE  4
typedef union {
    unsigned char s[WCH_SIZE];
    wchar_t wch;
} wch_t;
#endif

#define  CIN_CNAME_LENGTH       20      /* 輸入法中文名稱的最大長度 */
#define  N_CCODE_RULE           5       /* # of rules of encoding */
#define  N_KEYCODE              50      /* # of valid keys 0-9, a-z, .... */
#define  N_ASCII_KEY            95      /* Num of printable ASCII char */
#define  MODULE_ID_SIZE         20
#define  SYSCIN_VERSION         "20000210"

/* For encoding check. */
typedef struct {
    short n;
    u_char begin[N_CCODE_RULE], end[N_CCODE_RULE];
} charcode_t;

typedef struct {
    unsigned int total_char;
    u_char n_ch_encoding;
    charcode_t ccode[WCH_SIZE];
} ccode_info_t;

typedef struct xkeymap_s {
    u_char xkey;
    wch_t wch;
} xkeymap_t;

class TLS_CAsciiConvertor
{
private:
    /* key mapper table */
    xkeymap_t *fullchar;

    /* temp buffer for return */
    char cch[WCH_SIZE+1];

    /* each div table */
    char inpn_english[CIN_CNAME_LENGTH];
    char inpn_sbyte[CIN_CNAME_LENGTH];
    char inpn_2bytes[CIN_CNAME_LENGTH];
    wch_t ascii[N_ASCII_KEY];
    charcode_t ccp[WCH_SIZE];

private:
    void fullascii_init(wch_t *list);
public:
    TLS_CAsciiConvertor (char *szFileName);
    ~TLS_CAsciiConvertor ();

    /* public interface */
    char *szFullAsciiKeyStroke (u_char key);
    char *szFullCharKeyStroke (u_char key);
    char *szFullSymbolKeyStroke (u_char key);
};

#endif

