/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#ifndef __AHZINPUT_HPP__
#define __AHZINPUT_HPP__
#include <Phrase.h>
#include <ImmModule.h>
#include <TLS_ImmOp.hpp>

class TLS_CHzInput 
{
private:
    char tmpbuf[256];
    u_char IsHanziInput,
           IsFullChar,
           IsFullComma;
public:
    ImmOp_T *pImmSlib;
    u_long ClientType;
    u_char error;
    IMM_CLIENT *pClient;
private:
    PhraseItem *DupBufPhrase
            (PhraseItem *Old, PhraseItem *New,
             char *buf, int buflen, char **pNextPos);
public:
    TLS_CHzInput (ImmOp_T *pImmOp, char *szTableName, u_long type);
    ~TLS_CHzInput ();

    /* Input Method Operation */
        // return value:
        // 2 -- have filtered and translated
        // 1 -- have filtered
        // 0 -- not filtered
        // < 0 -- error code
    int KeyFilter (u_char key, char *buf, int *len);
    int FullCharFilter (u_char key, char *buf, int *len);
    int FullSymbolFilter (u_char key, char *buf, int *len);

    /* Input Method Mode Setting */
    int ResetInput ();
    int SetInputMode (long mode);

    /* Input Area Configuration & Operation */
    int ConfigInputArea (int SelectionLen);
    int GetInputDisplay (char *buf, long buflen);
    int GetSelectDisplay (char *buf, long buflen);

    /* Phrase Item Operations */
    int ModifyPhrase (long n, PhraseItem *p);
    int AppendPhrase (PhraseItem *p);
    int FlushUserPhrase ();
    PhraseItem *pGetSelectionItem (long n, PhraseItem *p, 
                                   char *buf, int len);
};

#endif

