
/*
**  UNICON - The Console Chinese & I18N 
**  Copyright (c) 1999-2002 Arthur Ma <arthur.ma@turbolinux.com.cn>
**
**  This file is part of UNICON, a console Chinese & I18N
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This library is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**  Lesser General Public License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
**  USA, or contact Ralf S. Engelschall <rse@engelschall.com>.
**
** TLC_SocketClient.hpp: SocketClient support
** See the file COPYING directory of this archive
** Author: see CREDITS
*/

#ifndef __SOCKET_CLIENT_HPP__
#define __SOCKET_CLIENT_HPP__

#include <stdio.h>

#define  SOCKET_OK            0
#define  SOCKET_CONNECT_FAIL  1
#define  SOCKET_TIME_OUT      2

class TLC_CSocketClient
{
private:
    int fd;
private:
    int PollRead (char *buf, int len);
    int PollWrite (char *buf, int len);
public:
    int ErrCode;
public:
    TLC_CSocketClient (char *szIpAddr, u_short port);
    ~TLC_CSocketClient ();
    int Write (char *buf, int len);
    int Read (char *buf, int max);
    char *GetErrorMsg (int ErrCode);
};

#endif

