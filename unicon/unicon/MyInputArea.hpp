/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#ifndef __INPUTAREA_HPP__
#define __INPUTAREA_HPP__

#include <MyTypes.h>
#include <Phrase.h>
#include <UniKey.hpp>

#define BLACK                  0x00
#define BLUE                   0x01
#define GREEN                  0x02
#define CYAN                   0x03
#define RED                    0x04
#define MAGENTA                0x05
#define BROWN                  0x06
#define LIGHT_GRAY             0x07
#define GRAY                   0x08
#define LIGHT_BLUE             0x09
#define LIGHT_GREEN            0x0a
#define LIGHT_CYAN             0x0b
#define LIGHT_RED              0x0c
#define LIGHT_MAGENTA          0x0d
#define YELLOW                 0x0e
#define WHITE                  0x0f

#define INPUT_BGCOLOR          (BLUE << 4)
#define INPUT_FGCOLOR          (BLUE << 4 | WHITE)
#define TITLE_FGCOLOR          (BLUE << 4 | LIGHT_CYAN)
#define MARK_FGCOLOR           (BLUE << 4 | LIGHT_GREEN)

#define MAX_SEL_PHRASE       20
#define MARK_LEN             4

#include <UniKey.hpp>
#include <MyConfig.hpp>
/* 
   SingleChar            <===========>     FullChar
   Symbol Status                           Symbol Status

 */
struct ImmState_T
{
    char IsFullSymbol;
};

class CMyInputArea
{
private:
    char *szMethod;
    CUniKey *pCMyVideo;
    CMyConfig *pTheConfig;
    MyVideoInfo_T MyVideoInfo;

    /* Output Buffer */
    char OutputBuffer[256];
    char SelectionBuffer[256];
    char InputBuffer[64];

    /* Input State Attribute */
    char IsFullChar;              /* 0 for half char, 1 for full char */
    ImmState_T ImmState[2];       /* 0 --- Single Char,  1 --- Double Char */
    char which;                   /* 0 --- Single Char,  1 -- Double Char */
    int IsFullSymbol;             /* 0 for half comma, 1 for full comma */
    int IsSysMenu;                /* 0 is no menu, 1 is menu */
    int IsHelpMenu;               /* 0 is no menu, 1 is menu */
    int IsFirstStartMsg;          /* 0 - no msg,   1 - msg   */

    int ShowTipItem;
    int nTty;
    int SelMaxLen;
    /* Input Area Attribute */
    int x1, y1, x2, y2;
    VColor bgcolor, fgcolor, markcolor;

public:
    CMyInputArea (int nTty, CMyConfig *pMyConfig, CUniKey *pCMyVideo);
    ~CMyInputArea ();

    /* Reset */
    void ResetMyConfig (CMyConfig *p);

    /* Input Area Output Operations */
    void InputAreaOutput (int x, char *string, VColor fg, VColor bg);
    void ShowFirstStartMsg ();
    void ShowHelpItem (int n);
    void ShowSysMenu ();

    /* Different Language Code support */ 
    void SetNewLanguageCode (char *szNewCode);

    /* Input Area Buffer Support */
    void SetInputDisplay (char *szStr);
    void SetSelectDisplay (char *szStr);

    /* Input Area Operation */
    void DisplaySelection (char *szStr);
    void ClearSelection ();
    void DisplayInput (char *szStr);
    void ClearInput ();
    void ClearInputArea ();
    void RefreshInputArea (int which, int IsFullSymbo);

    /* Set the Attribute */
    void SetInputArea (int x1, int y1, int x2, int y2);
    void SetNewMethod (char *szNewMethod);
    char *GetNewMethod ();
    void SetColors (VColor bgcolor, VColor fgcolor, VColor markcolor);

    /* Direct write string to Input Area */
    void WriteToInputArea (char *szStr);
    void WriteColorTextToInputArea (char *szStr);

    bool BeginAddPhrase ();
    bool UpAddPhrase (PhraseItem *p);
    bool DownAddPhrase (PhraseItem *p);
    bool EndAddPhrase ();
};

#endif

