/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#ifndef UNIDEV_H
#define UNIDEV_H

#include <linux/ioctl.h> 
// #include <video/fbcon.h> 

typedef struct __ChinesePut_T__
{
    int x, y;
    unsigned char c1, c2;
    long cl;
}
ChinesePut_T;

typedef struct __AsciiPut_T__
{
    int x, y;
    unsigned char ch;
    long cl;
}
AsciiPut_T;

typedef struct __VtInfo_T__
{
    int vt_has_resized;
    int width, height;
}
VtInfo_T;

typedef struct __VtFont_T__
{
    int tty;
    int font_type;
    int input_method_notify;
} VtFont_T;

#define DEVICE_FILE_NAME "/dev/unikey"
#define MAJOR_NUM 100
#define UNI_INPUT_GET_INFO    _IOR(MAJOR_NUM, 0, char *)
#define UNI_INPUT_SET_INFO    _IOR(MAJOR_NUM, 1, char *)
#define UNI_INPUT_REGISTER    _IOR(MAJOR_NUM, 2, char *)
#define UNI_INPUT_UNREGISTER  _IOR(MAJOR_NUM, 3, char *)

#define UNI_INPUT_PUT_ASCII   _IOR(MAJOR_NUM, 4, AsciiPut_T *)
#define UNI_INPUT_PUT_CHINESE _IOR(MAJOR_NUM, 5, ChinesePut_T *)
#define UNI_INPUT_CLS_BOTTOM  _IOR(MAJOR_NUM, 6, char *)
#define UNI_INPUT_GET_VT_INFO _IOR(MAJOR_NUM, 7, VtInfo_T *)

#define UNI_INPUT_SET_CUR_TTY _IOR(MAJOR_NUM, 8, VtInfo_T *)
#define UNI_INPUT_SET_RESIZE_FLAG _IOR(MAJOR_NUM, 9, char *)
#define UNI_INPUT_SET_UNRESIZE_FLAG _IOR(MAJOR_NUM, 10, char *)
#define UNI_SET_CURRENT_FONT  _IOR(MAJOR_NUM, 11, VtFont_T *)

#define MAXTTYS		6
#define SUCCESS         0
#define DEVICE_NAME     "/dev/unikey"
#define BUF_LEN         80
#define MAX_CHAR        8

/* op */
#define FLUSH_BOTTOM         1
#define FLUSH_INPUTMETHOD    2

typedef struct __TTY_KEY_T__
{
    u_char nTty;
    u_char op; //bFlushInput;
    u_char buf[MAX_CHAR];
    u_char nTotal;
}
TTY_KEY_T;
extern int nCurTty;
#endif

