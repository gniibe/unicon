/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#ifndef __HZINPUT_HPP__
#define __HZINPUT_HPP__

#include <stdlib.h>
#include <stdio.h>

#include <AImmHzInput.hpp>
#include <UniKey.hpp>
#include <ConfigManager.hpp>

/* define func */
#define INPUT_KEY_SPACE             0
#define INPUT_KEY_BACKSPACE         1
#define INPUT_KEY_ESCAPE            2
#define INPUT_KEY_PAGEUP            3
#define INPUT_KEY_PAGEDOWN          4
#define INPUT_KEY_1                 5
#define INPUT_KEY_2                 6
#define INPUT_KEY_3                 7   
#define INPUT_KEY_4                 8
#define INPUT_KEY_5                 9
#define INPUT_KEY_6                10
#define INPUT_KEY_7                11
#define INPUT_KEY_8                12
#define INPUT_KEY_9                13
#define INPUT_KEY_0                14
#define INPUT_METHOD_SELECTION_0   15
#define INPUT_METHOD_SELECTION_1   16
#define INPUT_METHOD_SELECTION_2   17
#define INPUT_METHOD_SELECTION_3   18
#define INPUT_METHOD_SELECTION_4   19
#define INPUT_METHOD_SELECTION_5   20
#define INPUT_METHOD_SELECTION_6   21
#define INPUT_METHOD_SELECTION_7   22
#define INPUT_METHOD_SELECTION_8   23
#define INPUT_METHOD_SELECTION_9   24
#define INPUT_TOGGLE               25
#define FULLCHAR_TOGGLE            26
#define SYS_MENU_TRIGGER           27
#define FULLSYMB_TOGGLE            28
#define HELP_TRIGGER               29

struct KeyTable
{
    u_char key;
    u_long func;
};

class CImmHzInput: public CAImmHzInput
{
private:
    /* Input Key Filters */
    int ExecPreFuncKey (u_char key);
    int ExecInputFuncKey (u_char func);
    int ExecSystemFuncKey (u_char func);

    /* Help & Sys Menu Key Filters */
    int DoSelectMethod (int method);
    int SendSysMenuKey (unsigned key);
    int SendSysHelpKey (unsigned char key);
    
    /* Input State Control */
    void SwitchHelpToNormal (void);
    void ToggleInputMethod(void);
    void ToggleHalfFull(void);
    void ToggleUserDefinePhrase ();
public:
    CImmHzInput (int nTty, ImmServer_T ImmServer, 
                 CConfigManager *pMyConfig, CUniKey *pCMyKey);
    ~CImmHzInput ();
    int  AllKeyFilter (u_char key, char *buf, int *len);
    int  KeyFilters (u_char *key, int total);
    void DoSwitchLang (int Tty, int coding);
    void PrintAll ();
};

#endif

