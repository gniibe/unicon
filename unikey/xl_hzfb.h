/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#ifndef __XL_HZFB_H__

typedef struct _hzfb 
{
    //geometry
    u_int width;                //real screen
    u_int height;

    //memory
    unsigned char *fb_mem;
    u_int mem_len;

    //graphics
    u_int bits_per_pixel;
    u_int line_length;
} HzFb_T;

extern int HzFbInit(HzFb_T *fbinfo);
extern int HzFbExit(HzFb_T *fbinfo);
extern void FbPutAscii (HzFb_T *fbinfo, int x, int y, 
                        long color, unsigned char c);
extern void FbPutChinese (HzFb_T *fbinfo, int x, int y,
                        long color, unsigned char c1, unsigned char c2);
extern void FbClearRect (HzFb_T *fbinfo, unsigned char color, int y1, int y2);
extern int GetFbHeight (HzFb_T *fbinfo);
extern int GetFbWidth (HzFb_T *fbinfo);
extern int GetCurrentTTY (void);

#endif /* __XL_HZFB_H__ */


