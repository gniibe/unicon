/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#include <linux/sched.h>
#include <linux/tty.h>
#include <linux/tty_flip.h>
#include <linux/errno.h>
#include <linux/kd.h>
#include <linux/malloc.h>
#include <linux/mm.h>
#include <linux/fb.h>
#include <linux/console_struct.h>
#include <video/fbcon.h>

#include "xl_hzfb.h"
#include <linux/fb_doublebyte.h>

int GetCurrentTTY (void)
{
   struct vc *q = &vc_cons [fg_console];
   struct vc_data *d = q->d;
   int unit = d->vc_num;

   return (unit + 1);
}

int HzFbInit (HzFb_T *fbinfo)
{
   struct vc *q = &vc_cons [fg_console];
   struct vc_data *d = q->d;
   int unit = d->vc_num;
   struct display *p = &fb_display [unit];
 
   fbinfo->width = p->var.xres;
   fbinfo->height = p->var.yres; 
   fbinfo->fb_mem = p->screen_base;
   fbinfo->mem_len = d->vc_screenbuf_size;
   fbinfo->bits_per_pixel = p->var.bits_per_pixel;
   fbinfo->line_length = p->line_length;

   return 0;
}

int HzFbExit (HzFb_T *fbinfo)
{
    return 0;
}

void FbPutAscii (HzFb_T *fbinfo, int x0, int y0, long color, unsigned char ch)
{
    struct vc *q = &vc_cons [fg_console];
    struct vc_data *conp = q->d;
    int c, ypos, xpos;

    c = ch | (color & 0xff) << 8; 
    xpos = x0; 
    ypos = y0;

    fbcon_putc_tl(conp, c, ypos, xpos);
}
 
void FbPutChinese (HzFb_T *fbinfo, int x0, int y0, 
	           long color, unsigned char ch, unsigned char cl)
{
    struct vc *q = &vc_cons [fg_console];
    struct vc_data *conp = q->d;
    int c, ypos, xpos;

    c = ch | ((color & 0xff) << 8)| ((cl+DB_LEFT) << 16) ;
    xpos = x0;
    ypos = y0;

    fbcon_putc_tl(conp, c, ypos, xpos);

    xpos ++;
    c = cl | ((color & 0xff) << 8)| ((ch+DB_RIGHT) << 16) ;
    fbcon_putc_tl(conp, c, ypos, xpos);
}

void FbClearRect (HzFb_T *fbinfo, unsigned char cl, int y1, int y2)
{
   int i;
   for (i = 0; i < fbinfo->width/8; i++) 
       FbPutAscii (fbinfo, i, y1, cl, ' ');
}

int GetFbHeight (HzFb_T *fbinfo)
{
    return fbinfo->height; 
}

int GetFbWidth (HzFb_T *fbinfo)
{
    return fbinfo->width; 
}

