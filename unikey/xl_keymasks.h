/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#ifndef __XL_KEYMASKS_H
#define __XL_KEYMASKS_H

#include <linux/fs.h>
#include <linux/wrapper.h>

#include "xl_key.h"
#include "unikey.h"

#define  LR_CTRL_KEY_DOWN            0x1d
#define  LR_CTRL_KEY_UP              (LR_CTRL_KEY_DOWN + 0x80)

#define  L_SHIFT_KEY_DOWN            0x2a
#define  L_SHIFT_KEY_UP              (L_SHIFT_KEY_DOWN + 0x80)
#define  R_SHIFT_KEY_DOWN            0x36
#define  R_SHIFT_KEY_UP              (R_SHIFT_KEY_DOWN + 0x80)

#define  L_ALT_KEY_DOWN              0x38
#define  L_ALT_KEY_UP                (L_ALT_KEY_DOWN + 0x80)
#define  NUMLOCK_DOWN                0x45
#define  NUMLOCK_UP                  0xC5

#define  S_KEY_1_DOWN                0x02
#define  S_KEY_2_DOWN                0x03
#define  S_KEY_3_DOWN                0x04
#define  S_KEY_4_DOWN                0x05
#define  S_KEY_5_DOWN                0x06
#define  S_KEY_6_DOWN                0x07
#define  S_KEY_7_DOWN                0x08
#define  S_KEY_8_DOWN                0x09
#define  S_KEY_9_DOWN                0x0a
#define  S_KEY_0_DOWN                0x0b

#define  S_KEY_A_DOWN                0x1e
#define  S_KEY_X_DOWN                0x2d
#define  S_KEY_P_DOWN                0x19
#define  S_KEY_N_DOWN                0x31
#define  S_KEY_R_DOWN                0x13

#define SPACE_KEY_DOWN               0x39
#define F1_KEY_DOWN                  0x3b
#define F1_KEY_UP                    (0x3b + 0x80)

#define F2_KEY_DOWN                  0x3c
#define F3_KEY_DOWN                  0x3d
#define F4_KEY_DOWN                  0x3e
#define F5_KEY_DOWN                  0x3f
#define F6_KEY_DOWN                  0x40
#define F7_KEY_DOWN                  0x41

#define PERIOD_KEY_DOWN              0x34
#define TAB_KEY_DOWN                 0x0f
#define TAB_KEY_UP                   0x8f
//Rat:add 20010731
#define PLUS_KEY_DOWN                0x0d
#define COMMA_KEY_DOWN               0x33
#define HOME_KEY_DOWN                0x47
#define UP_ARROW_DOWN		     0x48
#define PAGEUP_KEY_DOWN              0x49
#define DOWN_ARROW_DOWN              0x50
#define LEFT_ARROW_DOWN              0x4b
#define RIGHT_ARROW_DOWN             0x4d
#define END_KEY_DOWN                 0x4f
#define PAGEDOWN_KEY_DOWN            0x51
#define INSERT_KEY_DOWN              0x52
#define DELETE_KEY_DOWN              0x53

#define ALPHA_G_DOWN                 0x22
#define ALPHA_H_DOWN                 0x23
#define ALPHA_J_DOWN                 0x24
#define ALPHA_K_DOWN                 0x25
#define ALPHA_V_DOWN                 0x2f
#define ALPHA_B_DOWN                 0x30

extern void NotifyTtyToUpdate (void);
extern short int bHasClosed[MAXTTYS];
extern void ClearButtonBar (void);
void OnTtyChangeUpdate (int nTty);

#endif
