
/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#include <linux/tty.h>
#include <linux/tty_flip.h>
#include <linux/kernel.h>
#include <linux/malloc.h>
#include <linux/major.h>
#include <linux/version.h>
#include <linux/string.h>
#include <linux/errno.h>

#include "unikey.h"

static int fnOurTtyKeyHook (struct tty_struct *tty,
                         unsigned char ch, char flag);
static struct tty_struct *pCurrentTty = NULL;
static int fnOurLowKeyHook (unsigned char ch);
extern void OnTtyChangeUpdate (int nTty);

extern int (*Unicon_fnKeyHook) (struct tty_struct *tty, \
                                unsigned char ch, char flag);
extern int (*Unicon_fnLowerKeyHook) (unsigned char ch);
extern void handle_scancode(unsigned char scancode, int down);
extern void (*Unicon_TtyChangeUpdate) (int nTty);
extern short int bHasClosed[MAXTTYS];

void Unicon_InitTTY (void)
{
    Unicon_fnKeyHook = fnOurTtyKeyHook;
    Unicon_fnLowerKeyHook = fnOurLowKeyHook;
    Unicon_TtyChangeUpdate = OnTtyChangeUpdate;
}

void Unicon_ExitTTY (void)
{
    Unicon_fnKeyHook = NULL;
    Unicon_fnLowerKeyHook = NULL;
    Unicon_TtyChangeUpdate = NULL;
}

int Unicon_fnSendKey (unsigned char ch, char flag)
{
   struct tty_struct *tty = pCurrentTty;
   char mbz = 0;
   if (tty == NULL) {
       return -1;
   }
   tty->ldisc.receive_buf(tty, &ch, &mbz, 1);
#ifdef DEBUG
   printk ("will sending %c, 0x%x\n", ch, ch);
#endif
   return 1;
}

int Unicon_fnSendKeys (int nTty, unsigned char *ch, int Total)
{
    int i;
#ifdef DEBUG
   printk ("will sending %d keys \n", Total);
#endif
    for (i = 0; i < Total; i++, ch++)
    {
        Unicon_fnSendKey (*ch, TTY_NORMAL);
    }
    return Total;
}

extern int nCurTty;
extern int bFunKeyPressed;

static int fnOurTtyKeyHook (struct tty_struct *tty,
                         unsigned char ch, char flag)
{
    extern void WriteTtyKey (int nTTY, unsigned char ch);
    extern int Device_Open;

    if (Device_Open == 0)
        return 0;
    if (bHasClosed [nCurTty - 1] == 1)
        return 0;
    if ( !(nCurTty >= 1 && nCurTty <= MAXTTYS))
        return 0;
    pCurrentTty = tty;
    if (bFunKeyPressed == 1)
        return 0;
#ifdef DEBUG
    printk ("tty ch = %c\n", ch);
#endif
    WriteTtyKey (nCurTty, ch);
    return 1; 
}

extern int ScancodeToKeycode (unsigned char scancode, 
                              unsigned char *keycode);
extern void WriteTtyKey (int nTTY, unsigned char ch);
extern int alt_shift_ctrl (unsigned char scancode);

static int fnOurLowKeyHook (unsigned char scancode)
{
    unsigned char keycode;
    extern int Device_Open;
    int t;

    if (Device_Open == 0)
        return 0;

    if (alt_shift_ctrl (scancode) == 1)
        return 0;

    if ( !(nCurTty >= 1 && nCurTty <= MAXTTYS))
         return 0;

    t = ScancodeToKeycode (scancode, &keycode);
    if (t == 3)  /* ignore the ctrl space */
         return 1;
    if (bHasClosed [nCurTty - 1] == 1)
        return 0;
    switch (t)
    {
        case 0:
            return 0;
        case 1:
            WriteTtyKey (nCurTty, keycode);
            return 1;
        case 2:
            WriteTtyKey (nCurTty, keycode);
            return 0;
        case 4: /* Ctrl F1 Support */
            WriteTtyKey (nCurTty, keycode);
            return 1;
    }
    return 0;
}


