/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

/* This is the main entry of font manager of UNICON */

#include <linux/proc_fs.h>
#include <linux/ctype.h>
#include <linux/console_struct.h>
#include <linux/malloc.h>
#include <linux/fb_doublebyte.h>


// #define __XL_DEBUG__
typedef struct DBFontRegister_T
{
    int font_type;
    struct double_byte *p;
} DBFont;

static DBFont TtyDbFont[MAX_TTY];
static DBFont aDbFont[MAX_FONT];

static void XL_InitDBFont ();
static int XL_RegisterDBFont (int font_type, struct double_byte *p);
static int XL_UnregisterDBFont (int font_type);
static int XL_IsAvailable (int font_type);
static struct double_byte *XL_GetTtyDBFont (int tty);
static int XL_SetTtyDbFont (int tty,  int font_type);
static void ReleaseAllTtyFont (int font_type);

static void XL_InitDBFont ()
{
#ifdef __XL_DEBUG__
    printk ("XL_InitDBFont () \n");
#endif
    memset (aDbFont, 0, sizeof (aDbFont) * MAX_FONT);
    memset (TtyDbFont, 0, sizeof (TtyDbFont) * MAX_TTY);
}

static int XL_RegisterDBFont (int font_type, struct double_byte *p)
{
#ifdef __XL_DEBUG__
    printk ("XL_RegisterDBFont (font_type=%d, double_byte=0x%x)\n", font_type, (long) p);
#endif
    if (font_type < 0 || font_type >= MAX_FONT)
        return 0;
    if (aDbFont[font_type].p != NULL)
        return 0;
    aDbFont[font_type].font_type = font_type;
    aDbFont[font_type].p = p;
    return 1;
}

static int XL_UnregisterDBFont (int font_type)
{
#ifdef __XL_DEBUG__
    printk ("XL_UnregisterDBFont (font_type=%d)\n", font_type);
#endif
    if (font_type < 0 || font_type >= MAX_FONT)
        return 0;
    ReleaseAllTtyFont (font_type);
    memset (&aDbFont[font_type], 0, sizeof (DBFont));
    return 1;
}

static int XL_IsAvailable (int font_type)
{
#ifdef __XL_DEBUG__
    printk ("XL_IsAvailable (font_type=%d)\n", font_type);
#endif
    if (font_type < 0 || font_type >= MAX_FONT)
        return 0;
    if (aDbFont[font_type].p != NULL)
        return 1;
    return 0;
}
 
static struct double_byte *XL_GetTtyDBFont (int tty)
{
#ifdef __XL_DEBUG__
    printk ("XL_GetTtyDBFont (tty=%d)\n", tty);
#endif
    if (tty < 0 || tty >= MAX_TTY)
        return NULL;
   if (TtyDbFont[tty].p != NULL)
       return TtyDbFont[tty].p;
   else
       return NULL;
} 

static int XL_SetTtyDbFont (int tty,  int font_type)
{
#ifdef __XL_DEBUG__
    printk ("XL_SetTtyDBFont (tty=%d, font_type=%d)\n", tty, font_type);
#endif
    if (font_type < 0 || font_type >= MAX_FONT)
        return 0;
    if (tty < 0 || tty >= MAX_TTY)
        return 0;
    if (XL_IsAvailable (font_type) == 0)
        return 0;
    TtyDbFont[tty].font_type = font_type;
    TtyDbFont[tty].p = aDbFont[font_type].p;
    return 1;
}

static void ReleaseAllTtyFont (int font_type)
{
    int i;
#ifdef __XL_DEBUG__
    printk ("ReleaseAllTtyFont (font_type=%d)\n", font_type);
#endif
    if (font_type < 0 || font_type >= MAX_FONT)
        return;
    for (i = 0; i < MAX_TTY; i++)
    {
        if (TtyDbFont[i].font_type == font_type)
            memset (&TtyDbFont[i], 0, sizeof (DBFont));
    }
}

static DBFontManager UniconFontManagerAbc = 
{
     /* init */
     XL_InitDBFont,           /* init */

     /* font manager */
     XL_RegisterDBFont,       /* register */
     XL_UnregisterDBFont,     /* unregister */
     XL_IsAvailable,          /* test available */

     /* tty font manager */
     XL_GetTtyDBFont,
     XL_SetTtyDbFont,
};

void UniconFontManagerOpen ()
{
#ifdef __XL_DEBUG__
    printk ("void UniconFontManagerOpen ()\n");
#endif
    UniconFontManager = &UniconFontManagerAbc; 
}

void UniconFontManagerClose ()
{
#ifdef __XL_DEBUG__
    printk ("void UniconFontManagerClose ()\n");
#endif
/*
    ReleaseAllTtyFont (XL_DB_GB);
    ReleaseAllTtyFont (XL_DB_GBK);
    ReleaseAllTtyFont (XL_DB_BIG5);
    ReleaseAllTtyFont (XL_DB_JIS);
    ReleaseAllTtyFont (XL_DB_KSCM);
 */
    UniconFontManager = 0;
}

